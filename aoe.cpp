#include<iostream>
#include<string>
#include<iomanip>
#include<vector>

using namespace std;

struct Edge{
	int dest=0;
	int cost=0;
};

int main(){
	int edge_num=0;
	int vertex_num=0;
	cin>>edge_num;
	int graph[edge_num][4];

	//get input
	for(int i=0;i<edge_num;i++){
		cin>>graph[i][0]>>graph[i][1]>>graph[i][2]>>graph[i][3];
		if(graph[i][1]>vertex_num)vertex_num=graph[i][1];
		if(graph[i][2]>vertex_num)vertex_num=graph[i][2];
	}

	vertex_num=vertex_num+1;

	//events
	int event[vertex_num][2];
	for(int i=0;i<vertex_num;i++){
		event[i][0]=0;
		event[i][1]=__INT_MAX__;
	}

	//get every vertex`s ee
	for(int i=0;i<edge_num;i++){
		if(event[graph[i][2]][0]<event[graph[i][1]][0]+graph[i][3])event[graph[i][2]][0]=event[graph[i][1]][0]+graph[i][3];
	}

	//ee=le at destination 
	event[vertex_num-1][1]=event[vertex_num-1][0];

	//get every vertex`s le
	for(int i=edge_num-1;i>=0;i--){
		if(event[graph[i][1]][1]>event[graph[i][2]][1]-graph[i][3])event[graph[i][1]][1]=event[graph[i][2]][1]-graph[i][3];
	}

	//actives
	int active[edge_num][4];
	for(int i=0;i<edge_num;i++){
		active[i][0]=0;
		active[i][1]=0;
		active[i][2]=0;
	}
	for(int i=0;i<edge_num;i++){
		active[i][0]=event[graph[i][1]][0];
		active[i][1]=event[graph[i][2]][1]-graph[i][3];
		//cout<<active[i][0]<<" "<<active[i][1]<<endl;
	}

	cout<<setw(5)<<"event"<<setw(5)<<"ee"<<setw(5)<<"le"<<endl;
	for(int i=0;i<vertex_num;i++){
		cout<<setw(5)<<i<<setw(5)<<event[i][0]<<setw(5)<<event[i][1]<<endl;
	}

	cout<<setw(5)<<"act."<<setw(5)<<"e"<<setw(5)<<"l"<<setw(8)<<"slack"<<setw(10)<<"critical"<<endl;
	for(int i=0;i<edge_num;i++){
		cout<<setw(5)<<i+1<<setw(5)<<active[i][0]<<setw(5)<<active[i][1]<<setw(8)<<active[i][1]-active[i][0];
		if(active[i][1]-active[i][0]==0)cout<<setw(10)<<"Yes"<<endl;
		else cout<<setw(10)<<"No"<<endl;
	}

	for(int i=0;i<edge_num;i++){
		if(active[i][1]-active[i][0]==0)cout<<i+1<<" ";
	}

	return 0;
}
